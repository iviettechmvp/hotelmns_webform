﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Site.Master" CodeBehind="BookingService.aspx.cs" Inherits="HotelMNS_Web.BookingService" EnableEventValidation="false" ClientIDMode="Static"%>

<asp:Content ID="BookingServiceHeader" ContentPlaceHolderID="HeaderContent" runat="server">
        <div class="pull-right">
        <div class="pull-left">
            <nav class="nav">
                <ul id="navigate" class="sf-menu navigate">
                    <li><a href="Default.aspx">HOMEPAGE</a>
                    </li>
                    <li><a href="Accommondation.aspx">ACCOMMODATION</a>
                    </li>
                    <li><a href="News.aspx">NEWS</a>
                    </li>
                    <li><a href="About.aspx">ABOUT</a></li>
                    <li><a href="Contact.aspx">CONTACT</a></li>
                </ul>
            </nav>
        </div>
    </div>
</asp:Content>
<asp:Content ID="BookingServiceContent" ContentPlaceHolderID="MainContent" runat="server">
   <div class="breadcrumb breadcrumb-1 pos-center">
                <h1 style="color:white;">CHECK YOUR INFORMATION</h1>
            </div>
   <div class="content">
                <!-- Content Section -->
                <div class="container margint60">
                    <div class="row">
                        <div class="col-lg-9">
                            <!-- Explore Rooms -->
                            <table>
                                <tr class="products-title">
                                    <td class="table-products-image pos-center">
                                        <h6>IMAGE</h6>
                                    </td>
                                    <td class="table-products-name pos-center">
                                        <h6>ROOM NAME</h6>
                                    </td>
                                    <td class="table-products-price pos-center">
                                        <h6>PRICE</h6>
                                    </td>
                                    <td class="table-products-total pos-center">
                                        <h6>INFO</h6>
                                    </td>
                                </tr>

                                <tr class="table-products-list pos-center">
                                    <td class="products-image-table">
                                        <img alt="Products Image 1" src="<%="Image/"+resultImageroom[0].path_image%>" class="img-responsive"></td>
                                    <td class="title-table">
                                        <div class="room-details-list clearfix">
                                            <div class="pull-left">
                                                <h5><%=roomName%></h5>
                                            </div>
                                            <div class="pull-left room-rating">
                                                <ul>
                                                    <li><i class="fa fa-star"></i></li>
                                                    <li><i class="fa fa-star"></i></li>
                                                    <li><i class="fa fa-star"></i></li>
                                                    <li><i class="fa fa-star"></i></li>
                                                    <li><i class="fa fa-star inactive"></i></li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="list-room-icons clearfix">
                                            <ul>
                                                <%
                                                    foreach (var ser in listservice)
                                                    {
                                                %>
                                                <li><i class="<%=ser.image%>"></i></li>
                                                <% } %>
                                                <%
                                                    foreach (var fac in listFacilities)
                                                    {
                                                %>
                                                <li><i class="<%=fac.image %>"></i></li>
                                                <% } %>
                                            </ul>
                                        </div>
                                        <p> Date Stay : <i class="fa fa-calendar"></i> <%=shDateArrival.ToShortDateString()%> - <%=shDateDepart.ToShortDateString() %> </p>
                                        <p>Adult Number : <i class="fa fa-male"></i> -  <%=shAdultNumber.ToString() %></p>
                                        <p>Children Number : <i class="fa fa-child"></i> - <%=shChilNumber.ToString() %></p>
                                    </td>
                                    <td>
                                        <h3><%=shroomPrice.ToString()%>$<span class="room-bottom-time"">/<span style="letter-spacing:2px;"><%=countNight.ToString()%>nights</span></span></h3>
                                    </td>
                                    <td>
                                        <h4><%=RoomCount.ToString() %> Room</h4>
                                    </td>
                                </tr>
                            </table>
                           <h2 class="margint30"> Please Fill Your Info To Make Us Server You Better</h2>
                           <div class="contact-form margint30"><!-- Contact Form -->
                                <div >
                                    <asp:TextBox ID="txtFirstName" runat="server" type="text" placeholder="Frist Name" name="name" />
                                    <asp:TextBox ID="txtLasName" runat="server" placeholder="Last Name" name="name" />
                                    <div class="select-gender">
                                    <asp:DropDownList ID="dropdownGender" runat="server" class="slect1">
                                      <asp:ListItem value="Male" Selected="true">Male</asp:ListItem>
                                      <asp:ListItem value="Female">Female</asp:ListItem>
                                    </asp:DropDownList>
                                     <asp:DropDownList ID="dropdownCity" runat="server" class="slect2">
                                         <asp:ListItem value="Da Nang">Da Nang</asp:ListItem>
                                         <asp:ListItem value="Ha Noi">Ha Noi</asp:ListItem>
                                          <asp:ListItem value="Ho Chi MInh">Ho Chi Minh</asp:ListItem>
                                          <asp:ListItem value="Hai Phong">Hai Phong</asp:ListItem>
                                    </asp:DropDownList>
                                    </div>
                                    <asp:TextBox ID="txtEmail" runat="server" type="text" placeholder="E-Mail" name="email" />
                                    <asp:TextBox ID="txtPhoneNumber" runat="server" type="text" placeholder="Phone Number" name="Phone Number" />
                                    <asp:TextBox ID="txtNote" TextMode="multiline" runat="server" CssClass="note-form" placeholder="Write what do you want..." name="message"/>
                                    <asp:ScriptManager runat="server" ID="sm"></asp:ScriptManager>
                                    <asp:updatepanel runat="server"><ContentTemplate>
                                    <asp:Button ID="btnBookingRoom" runat="server" class="pull-right margint10" type="submit" Text="SUBMIT" OnClick="btnBookingRoom_Click"/>
                                    </ContentTemplate></asp:updatepanel>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3">
                            <!-- Sidebar -->
                            <div class="luxen-widget news-widget">
                                <div class="title-quick marginb20">
                                    <h5>HOTEL INFORMATION</h5>
                                </div>
                                <p>Curabitur blandit tempus porttitor. Nulla vitae elit libero, a pharetra augue. Lorem ipsumero, a pharetra augue. Lorem ipsum dolor sit amet, consectedui.</p>
                            </div>
                            <div class="luxen-widget news-widget">
                                <div class="title">
                                    <h5>CONTACT</h5>
                                </div>
                                <ul class="footer-links">
                                    <li>
                                        <p><i class="fa fa-map-marker"></i>Lorem ipsum dolor sit amet lorem Victoria 8011 Australia </p>
                                    </li>
                                    <li>
                                        <p><i class="fa fa-phone"></i>+61 3 8376 6284 </p>
                                    </li>
                                    <li>
                                        <p><i class="fa fa-envelope"></i>info@2035themes.com</p>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
</asp:Content>
