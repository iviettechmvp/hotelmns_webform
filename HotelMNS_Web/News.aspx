﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="News.aspx.cs" Inherits="HotelMNS_Web.News" EnableEventValidation="false" ClientIDMode="Static" %>

<asp:Content ID="AccomondationHeader" ContentPlaceHolderID="HeaderContent" runat="server">
    <div class="pull-right">
        <div class="pull-left">
            <nav class="nav">
                <ul id="navigate" class="sf-menu navigate">
                    <li><a href="Default.aspx">HOMEPAGE</a>
                    </li>
                    <li><a href="Accommondation.aspx">ACCOMMODATION</a>
                    </li>
                    <li class="active"><a href="News.aspx">NEWS</a>
                    </li>
                    <li><a href="About.aspx">ABOUT</a></li>
                    <li><a href="Contact.aspx">CONTACT</a></li>
                </ul>
            </nav>
        </div>
        <div class="pull-right">
            <div class="button-style-1 margint45">
                <a href="Booking.aspx"><i class="fa fa-calendar"></i>BOOK NOW</a>
            </div>
        </div>
    </div>
</asp:Content>

<asp:Content ID="News" ContentPlaceHolderID="MainContent" runat="server">
    <div class="content">
        <!-- Content Section -->
        <div class="container">
            <div class="row">
                <div class="error-page-container pos-center">
                    <!-- 404 Page -->
                    <h2>THIS PAGE IS NOT EXIST</h2>
                    <div class="big-error-number margint80 marginb80">404</div>
                    <div class="button-style-1">
                        <h6><a href="Default.aspx">HOMEPAGE</a></h6>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
