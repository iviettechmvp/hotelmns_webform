﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BUS;
using DTA;

namespace HotelMNS_Web
{
    public partial class _Default : Page
    {
        RoomAvaiableBUS roomBus = new RoomAvaiableBUS();
        protected List<spGetServiceRoom_Result> resultServiceRoom;
        protected List<spGetFacilitiesRoom_Result> resultFacilitiesRoom;
        protected List<spGetAllRoom_Result> listRoom;
        protected List<spGetListImageRoom_Result> lisImageRoom;
        public DateTime DateArrival;
        public DateTime DateDepart;
        public int adulNumber;
        public int childNumber;
        public int totalDays;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                txtDateArrival.Text = DateTime.Now.ToShortDateString();
                txtDateDeparture.Text = DateTime.Now.AddDays(1).ToShortDateString();
                GetSomeBestRoom();
            }
        }

        protected void GetSomeBestRoom()
        {
            listRoom = roomBus.GetAllAccomondation();
        }
        protected void LoadServiceOfRoom(int? roomtypeId, int? categoryid)
        {
            resultServiceRoom = roomBus.GetServiceRoom(roomtypeId, categoryid);
        }
        public void LoadFacilitesOfRooms(int? roomtypeId, int? categoryid)
        {
            resultFacilitiesRoom = roomBus.GetFacilities(roomtypeId, categoryid);
        }

        protected void btnSubmitRoom_Click(object sender, EventArgs e)
        {
            DateTime dArrival = DateTime.Parse(txtDateArrival.Text);
            DateTime dDepart = DateTime.Parse(txtDateDeparture.Text);
            int roomCount = Int32.Parse(dropdownPickRoom.Text);
            string ChilNumber = dropdownChilNumber.SelectedValue;
            string AdulNum = dropdownAdultNumber.SelectedValue;
            Response.Redirect("Booking.aspx?dateArial=" + Server.UrlEncode(dArrival.ToShortDateString()) + "&dateDepart=" + Server.UrlEncode(dDepart.ToShortDateString()) + "&roomCount=" + roomCount.ToString() + "&adultNumber=" + AdulNum + "&chilNumber=" + ChilNumber);

        }

        public void LoadListImage(int? roomtypeid, int? roomcateid)
        {
            lisImageRoom = roomBus.GetImageRoom(roomtypeid, roomcateid);
        }
    }
}