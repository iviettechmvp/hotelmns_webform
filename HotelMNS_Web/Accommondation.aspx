﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Site.Master" CodeBehind="Accommondation.aspx.cs" Inherits="HotelMNS_Web.Accommondation" EnableEventValidation="false" ClientIDMode="Static"%>

<asp:Content ID="AccomondationHeader" ContentPlaceHolderID="HeaderContent" runat="server">
    <div class="pull-right">
        <div class="pull-left">
            <nav class="nav">
                <ul id="navigate" class="sf-menu navigate">
                    <li ><a href="Default.aspx">HOMEPAGE</a>
                    </li>
                    <li class="active"><a href="Accommondation.aspx">ACCOMMODATION</a>
                    </li>
                    <li><a href="News.aspx">NEWS</a>
                    </li>
                    <li><a href="About.aspx">ABOUT</a></li>
                    <li><a href="Contact.aspx">CONTACT</a></li>
                </ul>
            </nav>
        </div>
        <div class="pull-right">
            <div class="button-style-1 margint45">
                <a href="Booking.aspx"><i class="fa fa-calendar"></i>BOOK NOW</a>
            </div>
        </div>
    </div>
</asp:Content>

<asp:Content ID="Accomondation" ContentPlaceHolderID="MainContent" runat="server">
    <div class="breadcrumb breadcrumb-1 pos-center">
        <h1 style="color:white;">ACCOMMODATION</h1>
    </div>
    <div class="content">
        <!-- Content Section -->
        <div class="container margint60">
            <div class="row">
                <%foreach (var item in listRoom)
                    { %>
                <div class="col-lg-4 col-sm-6 clearfix">
                    <!-- Explore Rooms -->
                    <div class="home-room-box clearfix">
                        <div class="room-image">
                            <%LoadListRoomImage(item.roomtype_id, item.roomcategory_id); %>
                            <img alt="Room Images" class="img-responsive" src="<%="Image/"+resultImageroom[0].path_image%>">
                            <div class="home-room-details">
                                <h5><a href="#"><%=item.roomtype_name +" "+item.roomcategory_name %></a></h5>
                                <div class="pull-left">
                                    <ul>
                                        <%LoadServiceOfRoom(item.roomtype_id, item.roomcategory_id);
                                            foreach (var ser in resultServiceRoom)
                                            {%>
                                        <li><i class="<%=ser.image %>"></i></li>
                                        <%} %>
                                        <%LoadFacilitesOfRooms(item.roomtype_id, item.roomcategory_id);
                                            foreach (var fac in resultFacilitiesRoom)
                                            {%>
                                        <li><i class="<%=fac.image %>"></i></li>
                                        <%} %>
                                    </ul>
                                </div>
                                <div class="pull-right room-rating">
                                    <ul>
                                        <li><i class="fa fa-star"></i></li>
                                        <li><i class="fa fa-star"></i></li>
                                        <li><i class="fa fa-star"></i></li>
                                        <li><i class="fa fa-star"></i></li>
                                        <li><i class="fa fa-star inactive"></i></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="room-details">
                            <p><%=item.description %></p>
                        </div>
                        <div class="room-bottom">
                            <div class="pull-left">
                                <h4><%=item.price %>$<span class="room-bottom-time">/ Day</span></h4>
                            </div>
                            <div class="pull-right">
                                <div class="button-style-1">
                                    <a href='RoomDetail.aspx?roomtypeid=<%=item.roomtype_id%>&roomcateid=<%=item.roomcategory_id%>&roomtyename=<%=item.roomtype_name%>&roomcatename=<%=item.roomcategory_name %>'>DETAIL</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <%} %>
            </div>
        </div>
    </div>

</asp:Content>


