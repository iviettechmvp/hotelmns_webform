﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Site.Master" CodeBehind="RoomDetail.aspx.cs" Inherits="HotelMNS_Web.RoomDetail" ClientIDMode="Static" %>

<asp:Content ID="RoomDetailHeader" ContentPlaceHolderID="HeaderContent" runat="server">
	<div class="pull-right">
		<div class="pull-left">
			<nav class="nav">
				<ul id="navigate" class="sf-menu navigate">
					<li><a href="Default.aspx">HOMEPAGE</a>
					</li>
					<li class="active"><a href="Accommondation.aspx">ACCOMMODATION</a>
					</li>
					<li><a href="News.aspx">NEWS</a>
					</li>
					<li><a href="About.aspx">ABOUT</a></li>
					<li><a href="Contact.aspx">CONTACT</a></li>
				</ul>
			</nav>
		</div>
		<div class="pull-right">
			<div class="button-style-1 margint45">
				<a href="Booking.aspx"><i class="fa fa-calendar"></i>BOOK NOW</a>
			</div>
		</div>
	</div>
</asp:Content>
<asp:Content ID="roomDetailContent" ContentPlaceHolderID="MainContent" runat="server">
	<div class="breadcrumb breadcrumb-1 pos-center">
		<h1 style="text-transform: uppercase;color:white;"><%=roomName %> ROOM</h1>
	</div>
	<div class="content">
		<!-- Content Section -->
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<!-- Room Gallery Slider -->
					<div class="room-gallery">
						<div class="margint40 marginb20">
							<h4>PHOTO GALLERY</h4>
						</div>
						<div class="flexslider-thumb falsenav">
							<ul class="slides">
                                <%foreach (var item in resultImageRoom)
                                    {%>
								<li data-thumb="<%="Image/"+item.path_image%>">
									<img alt="Slider 1" class="img-responsive" src="<%="Image/"+item.path_image%>" /></li>
                                <%} %>
							</ul>
						</div>
					</div>
				</div>
				<div class="col-lg-9 clearfix">
					<!-- Room Information -->
					<div class="col-lg-8 clearfix col-sm-8">
						<h4>ROOM DESCRIPTION</h4>
						<p class="margint30"><%=resultCapaRoom.description %></p>

						<p>Curabitur blandit tempus porttitor. Maecenas sed diam eget risus varius blandit sit amet non magna. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus nibh Curabitur blandit tempus porttitor. Maecenas sed diam eget risus varius blandit sit amr. Maecenas sed diam eget risus varius blandit sit amet non magna. Fusce dapibus, tellus ac blandit tempus.</p>
					</div>
					<div class="col-lg-4 clearfix col-sm-4">
						<div class="room-services">
							<!-- Room Services -->
							<h4>SERVICES</h4>
							<ul class="room-services">
								<%foreach (var ser in resultServiceRoom)
									{%>
								<li><i class="<%=ser.image %>"></i><%=ser.name %></li>
								<%} %>
							</ul>
						</div>
						<div class="location-weather margint40">
							<!-- Room Weather -->
							<h4>PRICES</h4>
							<div id="pricesroom" class="margint10">
								<h3 style="color: #e4b248;"><%=resultCapaRoom.price %>$
											<span class="room-bottom-time">/ Day</span>
								</h3>

							</div>
						</div>
					</div>
					<div class="col-lg-12 clearfix margint40 room-single-tab">
						<!-- Room Tab -->
						<div class="tab-style-2 ">
							<ul class="tabbed-area tab-style-nav clearfix">
								<li class="active">
									<h6><a href="#tab1s">EXAMPLE TAB</a></h6>
								</li>
								<li class="">
									<h6><a href="#tab2s">OTHER TABS EXAMPLE</a></h6>
								</li>
								<li class="">
									<h6><a href="#tab3s">SUIT IT NOW</a></h6>
								</li>
							</ul>
							<div class="tab-content tab-style-content">
								<div class="tab-pane fade active in" id="tab1s">
									<div class="col-lg-3 margint30">
										<img alt="Tab Image" class="img-responsive" src="img/tab-image.jpg">
									</div>
									<div class="col-lg-9 margint30">
										<p>
											Lorem ipsum dolor sit amet, consectetur adipisicing elit. Labore nesciunt ad architecto enim dignissimos incidunt recusandae officia odit sapiente laudantium obcaecati maiores aspernatur fuga neque eum? Reiciendis sit est cum magnam quos tempore tempora esse quibusdam culpa quisquam debitis eveniet necessitatibus excepturi sed ab cumque laudantium. Fugit culpa expedita odio id temporibus laudantium sunt non nemo sapiente dolorum? Soluta incidunt debitis molestiae consectetur accusantium nulla aperiam ad repellendus quas assumenda eum fugiat commodi iusto corporis adipisci autem dolor rerum! Nulla blanditiis aut vel aperiam soluta placeat quia quae libero architecto officiis eius dolorem asperiores est illo praesentium. Sunt reprehenderit alias!
										</p>
									</div>
								</div>
								<div class="tab-pane fade" id="tab2s">
									<div class="col-lg-3 margint30">
										<img alt="Tab Image" class="img-responsive" src="img/tab-image.jpg">
									</div>
									<div class="col-lg-9 margint30">
										<p>
											Lorem ipsum dolor sit amet, consectetur adipisicing elit. Labore nesciunt ad architecto enim dignissimos incidunt recusandae officia odit sapiente laudantium obcaecati maiores aspernatur fuga neque eum? Reiciendis sit est cum magnam quos tempore tempora esse quibusdam culpa quisquam debitis eveniet necessitatibus excepturi sed ab cumque laudantium. Fugit culpa expedita odio id temporibus laudantium sunt non nemo sapiente dolorum? Soluta incidunt debitis molestiae consectetur accusantium nulla aperiam ad repellendus quas assumenda eum fugiat commodi iusto corporis adipisci autem dolor rerum! Nulla blanditiis aut vel aperiam soluta placeat quia quae libero architecto officiis eius dolorem asperiores est illo praesentium. Sunt reprehenderit alias!
										</p>
									</div>
								</div>
								<div class="tab-pane fade" id="tab3s">
									<div class="col-lg-3 margint30">
										<img alt="Tab Image" class="img-responsive" src="img/tab-image.jpg">
									</div>
									<div class="col-lg-9 margint30">
										<p>
											Lorem ipsum dolor sit amet, consectetur adipisicing elit. Labore nesciunt ad architecto enim dignissimos incidunt recusandae officia odit sapiente laudantium obcaecati maiores aspernatur fuga neque eum? Reiciendis sit est cum magnam quos tempore tempora esse quibusdam culpa quisquam debitis eveniet necessitatibus excepturi sed ab cumque laudantium. Fugit culpa expedita odio id temporibus laudantium sunt non nemo sapiente dolorum? Soluta incidunt debitis molestiae consectetur accusantium nulla aperiam ad repellendus quas assumenda eum fugiat commodi iusto corporis adipisci autem dolor rerum! Nulla blanditiis aut vel aperiam soluta placeat quia quae libero architecto officiis eius dolorem asperiores est illo praesentium. Sunt reprehenderit alias!
										</p>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-lg-3 clearfix">
					<!-- Sidebar -->
					<div class="luxen-widget news-widget">
						<div class="title-quick marginb20">
							<h5>HOTEL INFORMATION</h5>
						</div>
						<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged</p>
					</div>
					<div class="luxen-widget news-widget">
						<div class="title">
							<h5>CONTACT</h5>
						</div>
						<ul class="footer-links">
							<li>
								<p><i class="fa fa-map-marker"></i>Lorem ipsum dolor sit amet lorem Victoria 8011 Australia </p>
							</li>
							<li>
								<p><i class="fa fa-phone"></i>+61 3 8376 6284 </p>
							</li>
							<li>
								<p><i class="fa fa-envelope"></i>info@2035themes.com</p>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
</asp:Content>

