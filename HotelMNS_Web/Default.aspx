﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Site.Master" CodeBehind="Default.aspx.cs" Inherits="HotelMNS_Web._Default" EnableEventValidation="false" ClientIDMode="Static"%>

<asp:Content ID="HomePageNavbar" ContentPlaceHolderID="HeaderContent" runat="server">
    <div class="pull-right">
        <div class="pull-left">
            <nav class="nav">
                <ul id="navigate" class="sf-menu navigate">
                    <li class="active"><a href="Default.aspx">HOMEPAGE</a>
                    </li>
                    <li><a href="Accommondation.aspx">ACCOMMODATION</a>
                    </li>
                    <li><a href="News.aspx">NEWS</a>
                    </li>
                    <li><a href="About.aspx">ABOUT</a></li>
                    <li><a href="Contact.aspx">CONTACT</a></li>
                </ul>
            </nav>
        </div>
        <div class="pull-right">
            <div class="button-style-1 margint45">
                <a href="Booking.aspx"><i class="fa fa-calendar"></i>BOOK NOW</a>
            </div>
        </div>
    </div>
</asp:Content>

<asp:Content ID="HomePageContent" ContentPlaceHolderID="MainContent" runat="server">
  <div class="slider slider-home">
                <!-- Slider Section -->
                <div class="flexslider slider-loading falsenav">
                    <ul class="slides">
                        <li>
                            <div class="slider-textbox clearfix">
                                <div class="container">
                                    <div class="row">
                                        <div class="slider-bar pull-left">WELCOME TO LUXEN PREMIUM HOTEL </div>
                                        <div class="slider-triangle pull-left"></div>
                                    </div>
                                </div>
                                <div class="container">
                                    <div class="row">
                                        <div class="slider-bar-under pull-left">5 STAR SERVICE</div>
                                        <div class="slider-triangle-under pull-left"></div>
                                    </div>
                                </div>
                            </div>
                            <img alt="Slider 1" class="img-responsive" src="Image/hslider1.jpg" />
                        </li>
                        <li>
                            <div class="slider-textbox clearfix">
                                <div class="container">
                                    <div class="row">
                                        <div class="slider-bar pull-left">WELCOME TO LUXEN PREMIUM HOTEL</div>
                                        <div class="slider-triangle pull-left"></div>
                                    </div>
                                </div>
                                <div class="container">
                                    <div class="row">
                                        <div class="slider-bar-under pull-left">5 STAR SERVICE</div>
                                        <div class="slider-triangle-under pull-left"></div>
                                    </div>
                                </div>
                            </div>
                            <img alt="Slider 2" class="img-responsive" src="Image/hslider2.jpg" />
                        </li>
                    </ul>
                </div>
                <div class="book-slider">
                    <div class="container">
                        <div class="row pos-center">
                            <div class="reserve-form-area">
                                    <ul class="clearfix">
                                        <li class="li-input">
                                            <label>ARRIVAL</label>
                                            <asp:TextBox type="text" ID="txtDateArrival" runat="server" name="dpd1" class="date-selector" placeholder="&#xf073;" />
                                        </li>
                                        <li class="li-input">
                                            <label>DEPARTURE</label>
                                            <asp:TextBox type="text" ID="txtDateDeparture" runat="server" name="dpd2" class="date-selector" placeholder="&#xf073;" />
                                        </li>
                                        <li class="li-select">
                                            <label>ROOMS</label>
                                            <asp:DropDownList ID="dropdownPickRoom" runat="server" CssClass="pretty-select">
                                                <asp:ListItem Value="1" Selected="True">1</asp:ListItem>
                                                <asp:ListItem Value="2">2</asp:ListItem>
                                                <asp:ListItem Value="3">3</asp:ListItem>
                                                <asp:ListItem Value="4">4</asp:ListItem>
                                                <asp:ListItem Value="5">5</asp:ListItem>
                                            </asp:DropDownList>
                                        </li>
                                        <li class="li-select">
                                            <label>ADULT</label>
                                            <asp:DropDownList ID="dropdownAdultNumber" runat="server" CssClass="pretty-select">
                                                <asp:ListItem Value="1" Selected="True">1</asp:ListItem>
                                                <asp:ListItem Value="2">2</asp:ListItem>
                                                <asp:ListItem Value="3">3</asp:ListItem>
                                            </asp:DropDownList>
                                        </li>
                                        <li class="li-select">
                                            <label>CHILDREN</label>
                                            <asp:DropDownList ID="dropdownChilNumber" runat="server" CssClass="pretty-select">
                                                <asp:ListItem Value="0" Selected="True">0</asp:ListItem>
                                                <asp:ListItem Value="1">1</asp:ListItem>
                                                <asp:ListItem Value="2">2</asp:ListItem>
                                                <asp:ListItem Value="3">3</asp:ListItem>
                                            </asp:DropDownList>
                                        </li>
                                        <li>
                                            <div class="button-style-1 margint30">
                                                <asp:LinkButton ID="btnSubmitRoom" runat="server" OnClick="btnSubmitRoom_Click" CausesValidation="False"><i class="fa fa-search"></i>SEARCH</asp:LinkButton>
                                            </div>
                                        </li>
                                    </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="bottom-book-slider">
                    <div class="container">
                        <div class="row pos-center">
                            <ul>
                                <li>
                                    <%for (int i = 0; i < 5; i++)
                                        { %>
                                    <i class="fa fa-star"></i>
                                    <% }%>
                                    FIVE STARS
                                </li>
                                <li><i class="fa fa-coffee"></i>COFFEE & BREAKFAST FREE</li>
                                <li>
                                    <i class="fa fa-credit-card-alt"></i>FAST PAYMENT
                                </li>
                                <li><i class="fa fa-wifi"></i>FREE WI-FI ALL ROOM</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
  <div class="content">
    <!-- Content Section -->
    <div class="about clearfix">
                    <!-- About Section -->
                    <div class="container">
                        <div class="row">
                            <div class="about-title pos-center">
                                <h2>WELCOME TO PARADISE</h2>
                                <div class="title-shape">
                                    <img alt="Shape" src="img/shape.png">
                                </div>
                                <p>Nullam quis risus eget urna mollis ornare vel eu leo. Cras mattis consectetur purus sit amet fermentum. Praesent <span class="active-color">commodo</span> cursus magna, vel scelerisque nisl .Nulleget urna mattis consectetur purus sit amet fermentum</p>
                            </div>
                            <div class="otel-info margint60">
                                <div class="col-lg-4 col-sm-12">
                                    <div class="title-style-1 marginb40">
                                        <h5>GALLERY</h5>
                                        <hr>
                                    </div>
                                    <div class="flexslider">
                                        <ul class="slides">
                                            <li>
                                                <img alt="Slider 1" class="img-responsive" src="Image/gslider1.jpg" /></li>
                                            <li>
                                                <img alt="Slider 2" class="img-responsive" src="Image/gslider2.jpg" /></li>
                                            <li>
                                                <img alt="Slider 3" class="img-responsive" src="Image/gslider3.jpg" /></li>
                                             <li>
                                                <img alt="Slider 4" class="img-responsive" src="Image/gslider4.jpg" /></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-sm-6">
                                    <div class="title-style-1 marginb40">
                                        <h5>ABOUT US</h5>
                                        <hr>
                                    </div>
                                    <p>Sed posuere consectetur est at lobortis. Aenean lacinia bibendum nulla sed consectetur. Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget laci. Maecenas faucibus mollis interdum.</p>
                                    <p>Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fer condimentum nibh, ut fermentum massa justo sit amet risus. mentum massa justo sit amet risus.</p>
                                    <p>Fusce dapibus, tellus ac cursus commodo ut fermentum massa. mentum massa justo sit amet risus.</p>
                                </div>
                                <div class="col-lg-4 col-sm-6">
                                    <div class="title-style-1 marginb40">
                                        <h5>OUR NEWS</h5>
                                        <hr>
                                    </div>
                                    <div class="home-news">
                                        <div class="news-box clearfix">
                                            <div class="news-time pull-left">
                                                <div class="news-date pos-center">
                                                    <div class="date-day">
                                                        20<hr />
                                                    </div>
                                                    MAY
                                                </div>
                                            </div>
                                            <div class="news-content pull-left">
                                                <h6><a href="#">News from us from now</a></h6>
                                                <p class="margint10">Donec ullamcorper nulla non metus auctor fringilla. Donec sed odio dui <a class="active-color" href="#">[...]</a></p>
                                            </div>
                                        </div>
                                        <div class="news-box clearfix">
                                            <div class="news-time pull-left">
                                                <div class="news-date pos-center">
                                                    <div class="date-day">
                                                        20<hr />
                                                    </div>
                                                    MAY
                                                </div>
                                            </div>
                                            <div class="news-content pull-left">
                                                <h6><a href="#">News from us from now</a></h6>
                                                <p class="margint10">Donec ullamcorper nulla non metus auctor fringilla. Donec sed odio dui. Nulla vitae elit libero, a pharetra augue <a class="active-color" href="#">[...]</a></p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
    <div class="explore-rooms margint30 clearfix">
                    <!-- Explore Rooms Section -->
                    <div class="container">
                        <div class="row">
                            <div class="title-style-2 marginb40 pos-center">
                                <h3>EXPLORE ROOMS</h3>
                                <hr>
                            </div>
                            <div class="col-lg-12">
                                <div id="imagecarousel" class="carousel slide intro-room" data-interval="2000" data-ride="carousel">
                                    <div class="carousel-inner">
                                        <div class="item active">
                                            <div class="row">
                                                <div class="col-lg-4 col-sm-6">
                                                    <div class="home-room-box">
                                                        <div class="room-image">
                                                            <%LoadListImage(listRoom[0].roomtype_id, listRoom[0].roomcategory_id); %>
                                                            <img alt="Room Images" class="img-responsive" src="<%="Image/"+lisImageRoom[0].path_image%>">
                                                            <div class="home-room-details">
                                                                <h5><a href="#"><%=listRoom[0].roomtype_name + " " +listRoom[0].roomcategory_name%></a></h5>
                                                                <div class="pull-left">
                                                                    <ul>
                                                                        <%LoadServiceOfRoom(listRoom[0].roomtype_id, listRoom[0].roomcategory_id);
                                                                            foreach (var ser in resultServiceRoom)
                                                                            {%>
                                                                        <li><i class="<%=ser.image %>"></i></li>
                                                                        <%} %>
                                                                        <%LoadFacilitesOfRooms(listRoom[0].roomtype_id, listRoom[0].roomcategory_id);
                                                                            foreach (var fac in resultFacilitiesRoom)
                                                                            {%>
                                                                        <li><i class="<%=fac.image %>"></i></li>
                                                                        <%} %>
                                                                    </ul>
                                                                </div>
                                                                <div class="pull-right room-rating">
                                                                    <ul>
                                                                        <li><i class="fa fa-star"></i></li>
                                                                        <li><i class="fa fa-star"></i></li>
                                                                        <li><i class="fa fa-star"></i></li>
                                                                        <li><i class="fa fa-star"></i></li>
                                                                        <li><i class="fa fa-star inactive"></i></li>
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="room-details">
                                                            <p><%=listRoom[0].description %></p>
                                                        </div>
                                                        <div class="room-bottom">
                                                            <div class="pull-left">
                                                                <h4><%=listRoom[0].price %>$<span class="room-bottom-time">/ Day</span></h4>
                                                            </div>
                                                            <div class="pull-right">
                                                                <div class="button-style-1">
                                                                    <a href='RoomDetail.aspx?roomtypeid=<%=listRoom[0].roomtype_id%>&roomcateid=<%=listRoom[0].roomcategory_id%>&roomtyename=<%=listRoom[0].roomtype_name%>&roomcatename=<%=listRoom[0].roomcategory_name %>'>DETAIL</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-4 col-sm-6">
                                                    <div class="home-room-box">
                                                        <div class="room-image">
                                                            <%LoadListImage(listRoom[1].roomtype_id, listRoom[1].roomcategory_id); %>
                                                            <img alt="Room Images" class="img-responsive" src="<%="Image/"+lisImageRoom[1].path_image%>">
                                                            <div class="home-room-details">
                                                                <h5><a href="#"><%=listRoom[1].roomtype_name + " " +listRoom[1].roomcategory_name%></a></h5>
                                                                <div class="pull-left">
                                                                    <ul>
                                                                        <%LoadServiceOfRoom(listRoom[1].roomtype_id, listRoom[1].roomcategory_id);
                                                                            foreach (var ser in resultServiceRoom)
                                                                            {%>
                                                                        <li><i class="<%=ser.image %>""></i></li>
                                                                        <%} %>
                                                                        <%LoadFacilitesOfRooms(listRoom[1].roomtype_id, listRoom[1].roomcategory_id);
                                                                            foreach (var fac in resultFacilitiesRoom)
                                                                            {%>
                                                                        <li><i class="<%=fac.image %>""></i></li>
                                                                        <%} %>
                                                                    </ul>
                                                                </div>
                                                                <div class="pull-right room-rating">
                                                                    <ul>
                                                                        <li><i class="fa fa-star"></i></li>
                                                                        <li><i class="fa fa-star"></i></li>
                                                                        <li><i class="fa fa-star"></i></li>
                                                                        <li><i class="fa fa-star"></i></li>
                                                                        <li><i class="fa fa-star inactive"></i></li>
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="room-details">
                                                            <p><%=listRoom[1].description %></p>
                                                        </div>
                                                        <div class="room-bottom">
                                                            <div class="pull-left">
                                                                <h4><%=listRoom[1].price %>$<span class="room-bottom-time">/ Day</span></h4>
                                                            </div>
                                                            <div class="pull-right">
                                                                <div class="button-style-1">
                                                                    <a href='RoomDetail.aspx?roomtypeid=<%=listRoom[1].roomtype_id%>&roomcateid=<%=listRoom[1].roomcategory_id%>&roomtyename=<%=listRoom[1].roomtype_name%>&roomcatename=<%=listRoom[1].roomcategory_name %>'>DETAIL</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-4 col-sm-6">
                                                    <div class="home-room-box">
                                                        <div class="room-image">
                                                            <%LoadListImage(listRoom[2].roomtype_id, listRoom[2].roomcategory_id); %>
                                                            <img alt="Room Images" class="img-responsive" src="<%="Image/"+lisImageRoom[2].path_image%>">
                                                            <div class="home-room-details">
                                                                <h5><a href="#"><%=listRoom[2].roomtype_name + " " +listRoom[2].roomcategory_name%></a></h5>
                                                                <div class="pull-left">
                                                                    <ul>
                                                                        <%LoadServiceOfRoom(listRoom[2].roomtype_id, listRoom[2].roomcategory_id);
                                                                            foreach (var ser in resultServiceRoom)
                                                                            {%>
                                                                        <li><i class="<%=ser.image %>""></i></li>
                                                                        <%} %>
                                                                        <%LoadFacilitesOfRooms(listRoom[2].roomtype_id, listRoom[2].roomcategory_id);
                                                                            foreach (var fac in resultFacilitiesRoom)
                                                                            {%>
                                                                        <li><i class="<%=fac.image %>""></i></li>
                                                                        <%} %>
                                                                    </ul>
                                                                </div>
                                                                <div class="pull-right room-rating">
                                                                    <ul>
                                                                        <li><i class="fa fa-star"></i></li>
                                                                        <li><i class="fa fa-star"></i></li>
                                                                        <li><i class="fa fa-star"></i></li>
                                                                        <li><i class="fa fa-star"></i></li>
                                                                        <li><i class="fa fa-star inactive"></i></li>
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="room-details">
                                                            <p><%=listRoom[2].description %></p>
                                                        </div>
                                                        <div class="room-bottom">
                                                            <div class="pull-left">
                                                                <h4><%=listRoom[2].price %>$<span class="room-bottom-time">/ Day</span></h4>
                                                            </div>
                                                            <div class="pull-right">
                                                                <div class="button-style-1">
                                                                    <a href='RoomDetail.aspx?roomtypeid=<%=listRoom[2].roomtype_id%>&roomcateid=<%=listRoom[2].roomcategory_id%>&roomtyename=<%=listRoom[2].roomtype_name%>&roomcatename=<%=listRoom[2].roomcategory_name %>'>DETAIL</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="item">
                                            <div class="row">
                                                <div class="col-lg-4 col-sm-6">
                                                    <div class="home-room-box">
                                                        <div class="room-image">
                                                            <%LoadListImage(listRoom[3].roomtype_id, listRoom[3].roomcategory_id); %>
                                                            <img alt="Room Images" class="img-responsive" src="<%="Image/"+lisImageRoom[3].path_image%>">
                                                            <div class="home-room-details">
                                                                <h5><a href="#"><%=listRoom[3].roomtype_name + " " +listRoom[3].roomcategory_name%></a></h5>
                                                                <div class="pull-left">
                                                                    <ul>
                                                                        <%LoadServiceOfRoom(listRoom[3].roomtype_id, listRoom[3].roomcategory_id);
                                                                            foreach (var ser in resultServiceRoom)
                                                                            {%>
                                                                        <li><i class="<%=ser.image %>""></i></li>
                                                                        <%} %>
                                                                        <%LoadFacilitesOfRooms(listRoom[3].roomtype_id, listRoom[3].roomcategory_id);
                                                                            foreach (var fac in resultFacilitiesRoom)
                                                                            {%>
                                                                        <li><i class="<%=fac.image %>""></i></li>
                                                                        <%} %>
                                                                    </ul>
                                                                </div>
                                                                <div class="pull-right room-rating">
                                                                    <ul>
                                                                        <li><i class="fa fa-star"></i></li>
                                                                        <li><i class="fa fa-star"></i></li>
                                                                        <li><i class="fa fa-star"></i></li>
                                                                        <li><i class="fa fa-star"></i></li>
                                                                        <li><i class="fa fa-star inactive"></i></li>
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="room-details">
                                                            <p><%=listRoom[3].description %></p>
                                                        </div>
                                                        <div class="room-bottom">
                                                            <div class="pull-left">
                                                                <h4><%=listRoom[3].price %>$<span class="room-bottom-time">/ Day</span></h4>
                                                            </div>
                                                            <div class="pull-right">
                                                                <div class="button-style-1">
                                                                    <a href='RoomDetail.aspx?roomtypeid=<%=listRoom[3].roomtype_id%>&roomcateid=<%=listRoom[3].roomcategory_id%>&roomtyename=<%=listRoom[3].roomtype_name%>&roomcatename=<%=listRoom[3].roomcategory_name %>'>DETAIL</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-4 col-sm-6">
                                                    <div class="home-room-box">
                                                        <div class="room-image">
                                                            <%LoadListImage(listRoom[4].roomtype_id, listRoom[4].roomcategory_id); %>
                                                            <img alt="Room Images" class="img-responsive" src="<%="Image/"+lisImageRoom[4].path_image%>">
                                                            <div class="home-room-details">
                                                                <h5><a href="#"><%=listRoom[4].roomtype_name + " " +listRoom[4].roomcategory_name%></a></h5>
                                                                <div class="pull-left">
                                                                    <ul>
                                                                        <%LoadServiceOfRoom(listRoom[4].roomtype_id, listRoom[4].roomcategory_id);
                                                                            foreach (var ser in resultServiceRoom)
                                                                            {%>
                                                                        <li><i class="<%=ser.image %>""></i></li>
                                                                        <%} %>
                                                                        <%LoadFacilitesOfRooms(listRoom[4].roomtype_id, listRoom[4].roomcategory_id);
                                                                            foreach (var fac in resultFacilitiesRoom)
                                                                            {%>
                                                                        <li><i class="<%=fac.image %>""></i></li>
                                                                        <%} %>
                                                                    </ul>
                                                                </div>
                                                                <div class="pull-right room-rating">
                                                                    <ul>
                                                                        <li><i class="fa fa-star"></i></li>
                                                                        <li><i class="fa fa-star"></i></li>
                                                                        <li><i class="fa fa-star"></i></li>
                                                                        <li><i class="fa fa-star"></i></li>
                                                                        <li><i class="fa fa-star inactive"></i></li>
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="room-details">
                                                            <p><%=listRoom[4].description %></p>
                                                        </div>
                                                        <div class="room-bottom">
                                                            <div class="pull-left">
                                                                <h4><%=listRoom[4].price %>$<span class="room-bottom-time">/ Day</span></h4>
                                                            </div>
                                                            <div class="pull-right">
                                                                <div class="button-style-1">
                                                                    <a href='RoomDetail.aspx?roomtypeid=<%=listRoom[4].roomtype_id%>&roomcateid=<%=listRoom[4].roomcategory_id%>&roomtyename=<%=listRoom[4].roomtype_name%>&roomcatename=<%=listRoom[4].roomcategory_name %>'>DETAIL</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-4 col-sm-6">
                                                    <div class="home-room-box">
                                                        <div class="room-image">
                                                            <%LoadListImage(listRoom[5].roomtype_id, listRoom[5].roomcategory_id); %>
                                                            <img alt="Room Images" class="img-responsive" src="<%="Image/"+lisImageRoom[5].path_image%>">
                                                            <div class="home-room-details">
                                                                <h5><a href="#"><%=listRoom[5].roomtype_name + " " +listRoom[5].roomcategory_name%></a></h5>
                                                                <div class="pull-left">
                                                                    <ul>
                                                                        <%LoadServiceOfRoom(listRoom[5].roomtype_id, listRoom[5].roomcategory_id);
                                                                            foreach (var ser in resultServiceRoom)
                                                                            {%>
                                                                        <li><i class="<%=ser.image %>""></i></li>
                                                                        <%} %>
                                                                        <%LoadFacilitesOfRooms(listRoom[5].roomtype_id, listRoom[5].roomcategory_id);
                                                                            foreach (var fac in resultFacilitiesRoom)
                                                                            {%>
                                                                        <li><i class="<%=fac.image %>""></i></li>
                                                                        <%} %>
                                                                    </ul>
                                                                </div>
                                                                <div class="pull-right room-rating">
                                                                    <ul>
                                                                        <li><i class="fa fa-star"></i></li>
                                                                        <li><i class="fa fa-star"></i></li>
                                                                        <li><i class="fa fa-star"></i></li>
                                                                        <li><i class="fa fa-star"></i></li>
                                                                        <li><i class="fa fa-star inactive"></i></li>
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="room-details">
                                                            <p><%=listRoom[5].description %></p>
                                                        </div>
                                                        <div class="room-bottom">
                                                            <div class="pull-left">
                                                                <h4><%=listRoom[5].price %>$<span class="room-bottom-time">/ Day</span></h4>
                                                            </div>
                                                            <div class="pull-right">
                                                                <div class="button-style-1">
                                                                    <a href='RoomDetail.aspx?roomtypeid=<%=listRoom[5].roomtype_id%>&roomcateid=<%=listRoom[5].roomcategory_id%>&roomtyename=<%=listRoom[5].roomtype_name%>&roomcatename=<%=listRoom[5].roomcategory_name %>'>DETAIL</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <a href="#imagecarousel" class="carousel-control left" data-slide="prev">
                                        <span class="glyphicon glyphicon-chevron-left fixleft"></span>
                                    </a>
                                    <a href="#imagecarousel" class="carousel-control right" data-slide="next">
                                        <span class="glyphicon glyphicon-chevron-right fixright"></span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
  </div>
</asp:Content>