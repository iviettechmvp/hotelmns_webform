﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BUS;
using DTA;

namespace HotelMNS_Web
{
    public partial class Accommondation : System.Web.UI.Page
    {
        RoomAvaiableBUS roomBus = new RoomAvaiableBUS();
        protected List<spGetServiceRoom_Result> resultServiceRoom;
        protected List<spGetFacilitiesRoom_Result> resultFacilitiesRoom;
        protected List<spGetAllRoom_Result> listRoom;
        protected List<spGetListImageRoom_Result> resultImageroom;
        public DateTime DateArrival;
        public DateTime DateDepart;
        public int adulNumber;
        public int childNumber;
        public int totalDays;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
                LoadAllRoom();
        }
        protected void LoadAllRoom()
        {
            listRoom = roomBus.GetAllAccomondation();
        }
        protected void LoadServiceOfRoom(int? roomtypeId, int? categoryid)
        {
            resultServiceRoom = roomBus.GetServiceRoom(roomtypeId, categoryid);
        }
        public void LoadFacilitesOfRooms(int? roomtypeId, int? categoryid)
        {
            resultFacilitiesRoom = roomBus.GetFacilities(roomtypeId, categoryid);
        }
        public void LoadListRoomImage(int? roomtypeid, int? roomcateid)
        {
            resultImageroom = roomBus.GetImageRoom(roomtypeid, roomcateid);
        }
    }
}